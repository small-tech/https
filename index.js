import os from 'node:os'
import process from 'node:process'
import path from 'node:path'
import https from 'node:https'
import EventEmitter from 'node:events'

import AutoEncrypt from '@small-tech/auto-encrypt'
import AutoEncryptLocalhost from '@small-tech/auto-encrypt-localhost'

class Events extends EventEmitter {
  CREATING_SERVER = Symbol()
  SERVER_CREATED = Symbol()
  SERVER_CLOSED = Symbol()
}

export const events = new Events()

const AUTO_ENCRYPT_STAGING_SERVER_TYPE = 1

const DATA_HOME = path.join(
  process.env.XDG_DATA_HOME || process.env.HOME || os.homedir(),
  '.local',
  'share'
)

export const SMALL_TECH_HOME_PATH = path.join(DATA_HOME, 'small-tech.org')
export const DEFAULT_SETTINGS_PATH = path.join(SMALL_TECH_HOME_PATH, 'https')

// Only modify this instance of the https module with our own createServer method.
const smallTechHttps = Object.assign({}, https)

smallTechHttps.createServer = function (options, listener) {
    // The first parameter is optional. If omitted, listener should be passed as the first argument.
    if (typeof options === 'function') {
      listener = options
      options = {}
    }

    const localDomains = ['localhost', 'place1.localhost', 'place2.localhost', 'place3.localhost', 'place4.localhost']
    const isLocal = options.domains == undefined || options.domains.some(domain => localDomains.includes(domain))
    const serverScope =  isLocal ? 'local' : 'global'
    const settingsPath = options.settingsPath ? path.join(path.resolve(options.settingsPath), serverScope) : path.join(DEFAULT_SETTINGS_PATH, serverScope)
    options.settingsPath = settingsPath

    if (options.staging) { options.serverType = AUTO_ENCRYPT_STAGING_SERVER_TYPE }
    delete options.staging

    const autoEncryptScope = {
      local: AutoEncryptLocalhost,
      global: AutoEncrypt
    }

    const messageSuffix = {
      local: 'at localhost with locally-trusted certificates',
      global: 'with globally-trusted Let’s Encrypt certificates'
    }
    events.emit(events.CREATING_SERVER, `Creating server ${messageSuffix[serverScope]}.`)

    const server = autoEncryptScope[serverScope].https.createServer(options, listener)

    function emitCloseEvent() {
      events.emit(events.SERVER_CLOSED, 'Server closed.')
    }

    if (serverScope === 'global') {
      // Allow AutoEncrypt to perform clean-up (e.g., remove interval timer for renewal check, etc.)
      server.on('close', () => {
        AutoEncrypt.shutdown()
        emitCloseEvent()
      })
    } else {
      // No additional clean-up required for Auto Encrypt Localhost.
      server.on('close', emitCloseEvent)
    }

    events.emit(events.SERVER_CREATED, 'Created HTTPS server.')

    return server
}

export default smallTechHttps
