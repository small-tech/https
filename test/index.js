import path from 'node:path'
import fs from 'node:fs'

import test from 'tape'

import bent from 'bent'
import hostname from '@small-tech/cross-platform-hostname'

import https, { DEFAULT_SETTINGS_PATH } from '../index.js'

const CUSTOM_SETTINGS_PATH  = path.join(DEFAULT_SETTINGS_PATH, 'test')

const getHttpsString = bent('GET', 'string', { Connection: 'close' })

function setup () {
  const rmRfSync = path => fs.rmSync(path, { recursive: true, force: true })
  rmRfSync(path.join(DEFAULT_SETTINGS_PATH, 'global', 'staging'))
  rmRfSync(CUSTOM_SETTINGS_PATH)
}

async function testLocalAndGlobalServer (t, customSettingsPath = false) {
  const localOptions = customSettingsPath ? {} : {settingsPath: CUSTOM_SETTINGS_PATH}

  const globalOptions = {
    staging: true,
    domains: [hostname]
  }
  if (customSettingsPath) { globalOptions.settingsPath = CUSTOM_SETTINGS_PATH }

  // Test local server
  const localServer = https.createServer(localOptions, (_request, response) => {
    response.end('local server ok')
  })

  await new Promise((resolve, _reject) => {
    localServer.listen(443, () => {
      resolve()
    })
  })

  const localServerResponse = await getHttpsString('https://localhost')

  t.strictEquals(localServerResponse, 'local server ok', 'Local server response is as expected.')

  localServer.close()

  await new Promise((resolve, _reject) => localServer.on('close', resolve))

  // Test global server

  const globalServer = https.createServer(globalOptions, (_request, response) => response.end('global server ok'))

  await new Promise((resolve, _reject) => globalServer.listen(443, resolve))

  const globalServerResponse = await getHttpsString(`https://${hostname}`)

  t.strictEquals(globalServerResponse, 'global server ok', 'Global server response is as expected.')

  globalServer.close()

  await new Promise((resolve, _reject) => globalServer.on('close', resolve))
}


test('@small-tech/https', async t => {
  setup()

  //
  // Test using default settings path.
  //

  // Test initial run (provisioning).
  await testLocalAndGlobalServer(t)

  // Test subsequent run (certificate material provided from disk).
  await testLocalAndGlobalServer(t)

  //
  // Test using custom settings path.
  //

  // Test initial run (provisioning).
  await testLocalAndGlobalServer(t, /* customSettingsPath = */ true)

  // Test subsequent run (certificate material provided from disk).
  await testLocalAndGlobalServer(t, /* customSettingsPath = */ true)

  // Test alternative function signature (no options object).
  const localServer = https.createServer((_request, response) => {
    response.end('local server listener set ok')
  })

  await new Promise((resolve, _reject) => localServer.listen(443, resolve))

  const localServerResponse = await getHttpsString('https://localhost')

  t.strictEquals(localServerResponse, 'local server listener set ok', 'Alternative function signature works as expected.')

  localServer.close()

  await new Promise((resolve, _reject) => localServer.on('close', resolve))

  // Test alternative local domain (place2.localhost).

  const localServerWithAlternativeDomain = https.createServer({ domains: ['place2.localhost'] }, (_request, response) => {
    response.end('local server with alternative name ok')
  })

  await new Promise((resolve, _reject) => localServerWithAlternativeDomain.listen(443, resolve))

  const localServerWithAlternativeNameResponse = await getHttpsString('https://place2.localhost')

  t.strictEquals(localServerWithAlternativeNameResponse, 'local server with alternative name ok', 'Alternative domain name (place2.localhost) works as expected.' )

  localServerWithAlternativeDomain.close()

  await new Promise((resolve, _reject) => localServerWithAlternativeDomain.on('close', resolve))
  
  t.end()
})
