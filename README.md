# @small-tech/https

A batteries-included version of the standard Node.js `https` module.

Simply replace Node’s `https` module with `@small-tech/https` and get:

- Automatically-provisioned trusted local development TLS certificates via [Auto Encrypt Localhost](https://codeberg.org/small-tech/auto-encrypt-localhost)

- Automatically-provisioned [Let’s Encrypt](https://letsencrypt.org/) TLS certificates via [Auto Encrypt](https://codeberg.org/small-tech/auto-encrypt).

- Automatic HTTP to HTTPS forwarding.

That’s it.

___Note:__ This is a standard ECMAScript Modules (ESM; es6 modules) project. If you need to use legacy CommonJS, [please see the 2.x branch](https://github.com/small-tech/https/tree/2.x) which is deprecated but still receives bug fixes._

## System requirements

- Version 4.x, 5.x+: Node 18.2 LTS+
- Version 3.x: Node 16 LTS
- Version 2.x: CJS, Node 16 LTS

__Tested and supported on:__

- Linux (tested on Fedora Silverblue 37 and Ubuntu 22.04)
- macOS (tested on Intel: Monterey, M1: Ventura, M4: Sequoia)
- Windows (10 and 11 under Windows Terminal and with Windows PowerShell)

> 💡 On macOS, if you’re using a third-party terminal application like iTerm, you must give it Full Disk Access rights or @small-tech/https will fail to install the policy file inside Firefox when creating local development servers. You can do this on the latest version of the operating system by adding iTerm to the list at System Settings → Privacy & Security → Full Disk Access.

>  💡 On Windows, @small-tech/https will also run under WSL 2 but this is not recommended when creating local development servers as local development certificates will not be automatically installed in your Windows browsers for you since your guest Linux system knows nothing about and cannot configure your host Windows environment.

## Testing

To run the unit tests:

1. Install dependencies:

    ```shell
    npm install
    ```

2. Ensure your computer is reachable from the Internet via its hostname.

    > 💡The global tests provision Let’s Encrypt certificates using [Auto Encrypt](https://codeberg.org/small-tech/auto-encrypt)). For details on how to set up your machine for this, please see the [Tests section of the Auto Encrypt Developer Documentation](https://codeberg.org/small-tech/auto-encrypt/src/branch/main/developer-documentation.md#tests).

3. Run:

    ```shell
    npm test
    ```

    > 💡 To run tests with debug output, use the following command instead:
    >
    > ```shell
    > npm run test-debug
    > ```

## Like this? Fund us!

[Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.

## Audience

This is [small technology](https://small-tech.org/about/#small-technology).

If you’re evaluating this for a “startup” or an enterprise, let us save you some time: this is not the right tool for you. This tool is for individual developers to build personal web sites and apps for themselves and for others in a non-colonial manner that respects the human rights of the people who use them.

## Install

```sh
npm i @small-tech/https
```

## Examples

### At localhost with automatically-provisioned localhost development certificates.

```js
import https from '@small-tech/https'

const server = https.createServer((request, response) => {
  response.end('Hello, world!')
})

server.listen(443, () => {
  console.log(' 🎉 Server running at https://localhost.')
})
```

Hit `https://localhost` and you should see your site with locally-trusted TLS certificates.

> 💡 As of version 5.2.0, you can also use the localhost aliases _place1.localhost_ - _place4.localhost_ when testing the peer-to-peer features of [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/) apps.

@small-tech/https uses [Auto Encrypt Localhost](https://codeberg.org/small-tech/auto-encrypt-localhost) to create a local Certificate Authority (cA) and add it to the various trust stores. It then uses that CA to create locally-trusted TLS certificates that are automatically used by your server.

### At hostname with automatically-provisioned Let’s Encrypt certificates.

```js
import https from '@small-tech/https'
import os form 'os'

const hostname = os.hostname()
const options = { domains: [hostname] }

const server = https.createServer((request, response) => {
  response.end('Hello, world!')
})

server.listen(443, () => {
  console.log(` 🎉 Server running at https://${hostname}.`)
})
```

To provision globally-trusted Let’s Encrypt certificates, we additionally create an `options` object containing the domain(s) we want to support, and pass it as the first argument in the `createServer()` method.

@small-tech/https automatically provisions Let’s Encrypt certificates for you the first time your server is hit using [Auto Encrypt](https://codeberg.org/small-tech/auto-encrypt) (this first load will take longer than future ones). During this initial load, other requests are ignored. This module will also automatically renew your certificates as necessary in plenty of time before they expire.

You can find a version of this example in the `/example` folder. To download and run that version:

```sh
# Clone this repository.
git clone https://codeberg.org/small-tech/https.git

# Switch to the directory.
cd https

# Install dependencies.
npm i

# Run the example.
npm run example
```

## A note on Linux and the security farce that is “privileged ports”

Linux has an outdated feature dating from the mainframe days that requires a process that wants to bind to ports < 1024 to have elevated privileges. While this was a security feature in the days of dumb terminals, today it is a security anti-feature. (macOS has dropped this requirement as of macOS Mojave.)

On modern Linux systems, you can disable privileged ports like this:

```sh
sudo sysctl -w net.ipv4.ip_unprivileged_port_start=0
```

Or, if you want to cling to ancient historic relics like a conservative to a racist statue, ensure your Node process has the right to bind to so-called “privileged” ports by issuing the following command before use:

```sh
sudo setcap cap_net_bind_service=+ep $(which node)
```

If you are wrapping your Node app into an executable binary using a module like [Nexe](https://github.com/nexe/nexe), you will have to ensure that every build of your app has that capability set. For an example of how we do this in [Site.js](https://sitejs.org), [see this listing](https://source.ind.ie/site.js/app/blob/master/bin/lib/ensure.js#L124).

## Related projects

Lower-level:

### Auto Encrypt

- Source: https://codeberg.org/small-tech/auto-encrypt
- Package: [@small-tech/auto-encrypt](https://www.npmjs.com/package/@small-tech/auto-encrypt)

Automatically provisions and renews [Let’s Encrypt](https://letsencrypt.org) TLS certificates for [Node.js](https://nodejs.org) [https](https://nodejs.org/docs/latest-v22.x/api/https.html) servers (including [Kitten](https://kitten.small-web.org), Polka, Express.js, etc.)

### Auto Encrypt Localhost

- Source: https://codeberg.org/small-tech/auto-encrypt-localhost
- Package: [@small-tech/auto-encrypt-localhost](https://www.npmjs.com/package/@small-tech/auto-encrypt-localhost)

Automatically provisions and installs locally-trusted TLS certificates for Node.js https servers (including [Kitten](https://kitten.small-web.org), Polka, Express.js, etc.) in 100% JavaScript (without any native dependencies like mkcert and certutil).

Higher level:

### Kitten

- Site: https://kitten.small-web.org
- Source code: https://codeberg.org/kitten/app

A [Small Web](https://ar.al/2024/06/24/small-web-computer-science-colloquium-at-university-of-groningen/) development kit.

- Build using HTML, CSS, and JavaScript.
- Progressively enhance with [Streaming HTML](https://kitten.small-web.org/tutorials/streaming-html).
- Go beyond traditional web apps to create peer-to-peer 💕 Small Web apps.

## Copyright

&copy; 2019-present [Aral Balkan](https://ar.al), [Small Technology Foundation](https://small-tech.org).

Let’s Encrypt is a trademark of the Internet Security Research Group (ISRG). All rights reserved. Node.js is a trademark of Joyent, Inc. and is used with its permission. We are not endorsed by or affiliated with Joyent or ISRG.

## License

[AGPL version 3.0.](https://www.gnu.org/licenses/agpl-3.0.en.html)
