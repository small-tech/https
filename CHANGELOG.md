# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.3.2] - 2025-02-08

The return of OCSP!

### Fixed

  - Upgraded Auto Encrypt to 4.1.3. This reinstates OCSP support in servers with Let’s Encrypt certificates in order to support existing certificates. OCSP support will be removed from Auto Encrypt completely after May 7, 2025, at which point the 90-day renewal period on all existing certificates will have passed.

## [5.3.1] - 2025-02-07

Oopsie fixie.

### Fixed

  - Upgrade to Auto Encrypt 4.1.2 to fix failing _package.json_ read attempt.

### Improved

  - Specify minimum supported Node.js version as 18.20.0 (due to use of [import attributes](https://nodejs.org/docs/latest-v22.x/api/esm.html#import-attributes) in Auto Encrypt 4.1.2). (All tests passing on current Node LTS version 22.)

## [5.3.0] - 2025-02-07

[OCSP?](https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol) We don’t need no OCSP!

### Removed

  - [OCSP stapling](https://en.wikipedia.org/wiki/OCSP_stapling) support in Auto Encrypt (by upgrading to Auto Encrypt 4.1.1). (Because [Let’s Encrypt has removed OCSP support.](https://letsencrypt.org/2024/12/05/ending-ocsp/))

### Fixed

  - Client requests now send `Connection: close` header so tests are not tripped up by the default `keep-alive` introduced in Node 19.

## [5.2.0] - 2023-02-14

Localhost by any other name…

### Added

  - Localhost subdomains/aliases _place1.localhost_ - _place4.localhost_ are now recognised as local servers in addition to _localhost_ and a local server is created if the list of domains contains any one of them. (See [issue #1](https://codeberg.org/small-tech/https/issues/1).)

## [5.1.1] - 2023-02-14

Update Auto Encrypt Localhost to version 8.3.2.

### Changed

  - Dependency changed from syswide-cas to @small-tech/syswide-cas (our own fork as the original package is unmaintained).

  - Now properly added the Certificate Authority (CA) to the Node trust store on every run instead of only when the CA is created. (Unlike the system and Firefox trust stores, the Node trust store is not persisted and only lasts for as long as the current Node session.)

## [5.1.0] - 2023-02-10

Updated Auto Encrypt Localhost to version 8.3.0.

(Now with added subjectAltNames. You can use these IP and DNS Name aliases for localhost to test peer-to-peer Small Web site and applications locally.)

### Added

  - Add subject alt names for 127.0.0.2 - 127.0.0.4 (inclusive) and place1.localhost, place2.localhost, place3.localhost and place4.localhost to the server certificate.

## [5.0.2] - 2023-01-26

The rest of the Codeberg.

### Changed

  - Actually replaced GitHub URLs in Node package file with Codeberg ones.

## [5.0.1] - 2023-01-26

The tip of the Codeberg.

### Changed

  - Replaced GitHub URLs in Node package file with Codeberg ones.

## [5.0.0] - 2023-01-26

Updates Auto Encrypt Localhost to version 8.2.0.

While this isn’t a breaking change, since Auto Encrypt Localhost has been rewritten from scratch, I’m making this a major version update out of an abundance of caution.

The new change means that https no longer has any native dependencies (Auto Encrypt Localhost previously relied on mkcert and certutil).

### Changed

  - Update Auto Encrypt Localhost to version 8.2.0.
  - Change default settings path to `<DATA HOME>/.local/share/small-tech.org/https`
  - Replace log messages with events.
  - Remove dependency: fs-extra.
  - Update license terms from AGPL-3.0-or-later to AGPL-3.0.

## [4.0.0] - 2023-01-02

Breaking dependency update.

Requires Node 18.2 (LTS)+.

Adds M1 Mac and ARM on Windows support when running servers on localhost.

### Changed

  - __Breaking change:__ Update Auto Encrypt to 4.0.0.
  - Update Auto Encrypt Localhost to 7.2.0 (updates mkcert, etc.)

## [3.1.0] - 2022-06-07

Dependency update.

### Changed

  - Update Auto Encrypt to version 3.1.0. This updates the certificate signing request (CSR) signature algorithm from the obsolete SHA-1 to SHA-256. (Let’s Encrypt will beging to reject certificate requests signed with SHA-1 on September 15, 2022. See https://community.letsencrypt.org/t/rejecting-sha-1-csrs-and-validation-using-tls-1-0-1-1-urls/175144)

## [3.0.3] - 2021-03-24

A hybrid approach to local certificates.

### Changed

Upgrade Auto Encrypt Localhost to version 7.0.7.

This version implements a hybrid approach to mkcert installation and certificate authority and certificate creation that combines the best parts of the methods used in 6.x and 7.x.

Specifically:

  - __mkcert is now installed at post-install__ (which removes the requirement for the graphical sudo prompt, which was using pkexec, which behaves differently to sudo and was creating the certificate material files with the wrong permissions on Linux).

  - __root certificate authority and TLS certificates are created as necessary at runtime__ (while this requires the person to enter their sudo password, the prompt is shown in the command-line as expected unlike [the npm bug that was causing the prompt to be hidden when run in a lifecycle script](https://github.com/npm/cli/issues/2887)).


## [3.0.2] - 2021-03-23

Cross platform once again.

### Changed

  - Upgrade Auto Encrypt Localhost to version 7.0.5.

    This fixes installation on macOS (which was failing because of differences in how the graphical sudo prompt affects file permissions between Linux and macOS) and re-implements Windows support (tested/supported only on Windows 10, under Windows Terminal, with PowerShell).

  - Upgrade Auto Encrypt to version 3.0.1.

    This fixes a regression introduced on Windows in the 3.x branch due to the way in which `__dirname` was being defined.

  - Reduce package size (unpacked) from 51.2KB to 18.3KB by linking to AGPL license body instead of including the entire text of it.

## [3.0.1] - 2021-03-17

### Fixed

  - Hang during npm install due to npm bug in Auto Encrypt Localhost (AEL). Upgraded AEL to version 7.0.4 which includes a graphical sudo prompt workaround.

## [3.0.0] - 2021-03-09

__Breaking change:__ ESM version. Includes Auto Encrypt 3.0.0 and Auto Encrypt Localhost 7.0.2.

### Changed

  - Uses ECMAScript Modules (ESM; es6 modules).
  - Upgraded Auto Encrypt to version 3.0.0 (ESM + includes the latest Let’s Encrypt staging certificate authority root certificate for testing).
  - Upgraded Auto Encrypt Localhost to version 7.0.2 (the mkcert binaries are no longer bundled).
  - Testing and coverage: migrate from using nyc, tap-spec, and tap-nyc to c8 and tap-monkey.
  - Reduce npm package size by specifying files whitelist.

## [2.1.1] - 2021-02-16

### Changed

   - Upgrade Auto Encrypt to version 2.0.6. Fixes assignment to constant that would result in a crash when a Retry-After header was received from Let’s Encrypt.

## [2.1.0] - 2020-11-04

### Changed

  - Upgrade Auto Encrypt Localhost to version 6.1.0. (This upgrades mkcert to 1.4.2 and includes the new separate mkcert arm64 binary.)

## [2.0.0] - 2020-11-03

### Changed

  - __Breaking change:__ Update to Auto Encrypt Localhost version 6.0.0.

    Running multiple local servers at different HTTPS ports no longer results in an error due to port 80 being unavailable for the HTTP Server. However, know that only the first server will get the HTTP Server at port 80 that redirects HTTP calls to HTTPS and also serves your local root certificate authority public key.

## [1.6.1] - 2020-10-29

### Improved

  - Update dependencies to remove npm vulnerability warnings.

## [1.6.0] - 2020-07-11

### Added

  - Update Auto Encrypt Localhost to 5.4.0 to add arm64 support on Linux.

## [1.5.1] - 2020-07-10

### Fixed

  - Update Auto Encrypt to 2.0.4 to fix HTTP → HTTPS redirects on global servers. They now work (the HTTP server was not being started previously due to a typo in the monkey-patched method name).

## [1.5.0] - 2020-07-07

### Changed

  - Update Auto Encrypt Localhost to version 5.3.0 (now serves the local root certificate authority’s public key at route /.ca).

## [1.4.0] - 2020-07-06

### Changed

  - Update Auto Encrypt Localhost to version 5.2.2 (you can now access local servers from any device on your local network via its IPv4 address).

## [1.3.1] - 2020-07-03

### Changed

  - Update to Auto Encrypt version 2.0.1 (HTTP to HTTPS forwarding is now logged).

## [1.3.0] - 2020-07-03

### Changed

  - Update to Auto Encrypt version 2.0.0 with automatic HTTP to HTTPS forwarding for servers at hostname.

## [1.2.5] - 2020-06-20

### Changed

  - Update to Auto Encrypt version 1.0.3 with fix for carriage returns in CSRs causing some certificate provisioning attempts to fail.

## [1.2.4] - 2020-06-16

### Changed

  - Updated Auto Encrypt to version 1.0.2 and Auto Encrypt localhost to version 5.1.2 (fixes and cosmetic improvements).

## [1.2.3] - 2020-06-16

### Changed

  - Updated Auto Encrypt to version 1.0.1 and Auto Encrypt localhost to version 5.1.1.
  - Log output now conforms to format used by Site.js.

## [1.2.2] - 2020-04-16

### Added

  - Minor: clean up old (pre-version-1.2.0) certificate folders at startup.

## [1.2.1] - 2020-04-15

### Added

  - Minor: add repository link to package file.

## [1.2.0] - 2020-04-15

### Changed

  - Let’s Encrypt certificates are now managed by [Auto Encrypt](https://source.ind.ie/site.js/lib/Auto Encrypt).
  - Entire library is now licensed under AGPL version 3.0 or later.

## [1.1.0] - 2020-02-15

### Added

  - Support for QUIET=true environment variable for what was previously console.log() output.

## [1.0.9] - 2020-02-09

### Changed

  - Upgrade to version 3.1.7 of Nodecert (with fix to output formatting updates).

## [1.0.8] - 2020-02-09

### Changed

  - Upgrade to version 3.1.5 of Nodecert (with output formatting updates).

## [1.0.7] - 2020-02-09

### Changed

  - Update console output format to match the one used in Site.js (cosmetic).

## [1.0.6] - 2019-11-26

### Fixed

  - Update to latest Nodecert (version 3.1.4) which fixes regression so Node.js once again recognises local certificates as valid.

## [1.0.5] - 2019-11-26

### Fixed

  - Update to latest Nodecert (version 3.1.3) which fixes crash when multiple directories are missing in the requested Nodecert configuration directory.

## [1.0.4] - 2019-11-26

### Fixed

  - Setting a custom certificate directory no longer causes a crash.

## [1.0.3] - 2019-11-26

### Fixed

  - Found and removed two other functions where Greenlock was phoning home. Seriously, what’s wrong with you, AJ? FFS!

### Added

  - Now emits `server.SMALL_TECH_ORG_ERROR_HTTP_SERVER` event on HTTP (HTTP-01 and redirection) server error.

## [1.0.2] - 2019-11-25

  - `certificateDirectory` option for overriding the default certificate directory. Local certificates will be created in a `local` subdirectory of this directory and global certificates will be created in a `global` subdirectory of this directory.

## [1.0.1] - 2019-11-09

### Fixed

  - Missing files in `lib/` folder due to existence of second `package.json` file.

## [1.0.0] - 2019-11-08

### Added

  - Initial release.
